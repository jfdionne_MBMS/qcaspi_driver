#ifndef __ASM_ARCH_QCA_BOOTSTRAP_H__
#define __ASM_ARCH_QCA_BOOTSTRAP_H__

#define QCA_SPI_UART_SELECT    (1 << 0)
#define QCA_RS232_RS485_SELECT (1 << 1)
#define QCA_XCVR_ENABLED       (1 << 2)
#define QCA_DISABLE_OUTPUTS    (1 << 3)

#define QCA_HOST_MODE_SHIFT 4
#define QCA_HOST_MODE (0x3 << QCA_HOST_MODE_SHIFT)

#define QCA_HOST_MODE_DISABLED 0
#define QCA_HOST_MODE_UART 1
#define QCA_HOST_MODE_SPI 2

uint32_t qca_read_bootstrap(void);

#endif
